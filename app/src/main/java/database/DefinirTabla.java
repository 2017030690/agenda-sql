package database;

import android.provider.BaseColumns;

public class DefinirTabla {

    public DefinirTabla(){}

    public static abstract class Contacto implements BaseColumns{

        public static final String TABLE_NAME = "contactos";
        public static final String NOMBRE = "nombre";
        public static final String TELEFONO_1 = "telefono_1";
        public static final String TELEFONO_2 = "telefono_2";
        public static final String DOMICILIO = "domicilio";
        public static final String NOTA = "nota";
        public static final String FAVORITO = "favorito";
    }
}
