package com.example.agendasql;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import database.AgendaDB;
import database.Contacto;

public class MainActivity extends AppCompatActivity {

    //Variables
    private EditText txtNombre;
    private EditText txtTelefono1;
    private EditText txtTelefono2;
    private EditText txtDomicilio;
    private EditText txtNota;
    private CheckBox chkFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;

    private AgendaDB db;
    private Contacto savedContact;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtNombre = (EditText)findViewById(R.id.txtNombre);
        this.txtTelefono1 = (EditText)findViewById(R.id.txtTelefono1);
        this.txtTelefono2 = (EditText)findViewById(R.id.txtTelefono2);
        this.txtDomicilio = (EditText)findViewById(R.id.txtDomicilio);
        this.txtNota = (EditText)findViewById(R.id.txtNota);
        this.chkFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        this.btnGuardar = (Button)findViewById(R.id.btnGuardar);
        this.btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        this.btnListar = (Button)findViewById(R.id.btnListar);

        this.db = new AgendaDB(MainActivity.this);

        this.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarContacto();
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });

        this.btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    public void limpiarCampos(){
        this.txtNombre.setText("");
        this.txtTelefono1.setText("");
        this.txtTelefono2.setText("");
        this.txtDomicilio.setText("");
        this.txtNota.setText("");
        this.chkFavorito.setChecked(false);
        savedContact = null;
    }

    public boolean verificarCampos(){
        boolean b = true;
        if(txtNombre.getText().toString().trim().equals("")){
            Toast.makeText(MainActivity.this, "Favor de llenar el campo de nombre",Toast.LENGTH_SHORT).show();
            txtNombre.requestFocus();
            b = false;
        }
        else if(txtTelefono1.getText().toString().trim().equals("")){
            Toast.makeText(MainActivity.this, "Favor Ingresar un telefono",Toast.LENGTH_SHORT).show();
            txtTelefono1.requestFocus();
            b = false;
        }
        else if(txtDomicilio.getText().toString().trim().equals("")){
            Toast.makeText(MainActivity.this, "Favor de llenar el campo de domicilio",Toast.LENGTH_SHORT).show();
            txtDomicilio.requestFocus();
            b = false;
        }
        return b;
    }

    public void guardarContacto(){
        if(verificarCampos()){
            int favorito = 0;
            String nombre = txtNombre.getText().toString();
            String telefono1 = txtTelefono1.getText().toString();
            String telefono2 = txtTelefono2.getText().toString();
            String domicilio = txtDomicilio.getText().toString();
            String nota = txtNota.getText().toString();
            if(chkFavorito.isChecked()){
                favorito = 1;
            }
            db.openDatabase();
            Contacto contacto = new Contacto(nombre,telefono1,telefono2,domicilio,nota,favorito);
            if(savedContact == null){
                if(db.insertarContacto(contacto) > 0){
                    Toast.makeText(MainActivity.this,"Contacto agregado",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(MainActivity.this,"Error al agregar contacto",Toast.LENGTH_SHORT).show();
                }
            }
            else{
                db.actualizarContacto(contacto,id);
                Toast.makeText(MainActivity.this,"Contacto actualizado",Toast.LENGTH_SHORT).show();
            }
            db.cerrar();
            limpiarCampos();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto)data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = (int)contacto.getID();
            txtNombre.setText(contacto.getNombre());
            txtTelefono1.setText(contacto.getTelefono1());
            txtTelefono2.setText(contacto.getTelefono2());
            txtDomicilio.setText(contacto.getDomicilio());
            txtNota.setText(contacto.getNota());
            if(contacto.getFavorito() > 0){
                chkFavorito.setChecked(true);
            }
        }
    }
}