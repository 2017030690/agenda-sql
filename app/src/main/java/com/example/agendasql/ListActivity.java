package com.example.agendasql;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import database.AgendaDB;
import database.Contacto;

public class ListActivity extends AppCompatActivity {

    private Button btnNuevo;
    private ListView listView;

    private AgendaDB agendaDB;
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        btnNuevo = (Button)findViewById(R.id.btnNuevo);
        listView = (ListView)findViewById(R.id.list);

        agendaDB = new AgendaDB(this);
        listaContacto = new ArrayList<Contacto>();
        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.layout_contacto,listaContacto);
        listView.setAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void llenarLista() {
        agendaDB.openDatabase();
        listaContacto = agendaDB.allContactos();
        agendaDB.cerrar();
        if(listaContacto.size() > 0){
            Toast.makeText(ListActivity.this,"Si hay : " +listaContacto.size(),Toast.LENGTH_SHORT).show();
        }
    }


    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public MyArrayAdapter(@NonNull Context context, @LayoutRes int textViewResourceId, ArrayList<Contacto> contactos) {
            super(context, textViewResourceId,contactos);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.contactos = contactos;
            this.inflater =(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceId,parent,false);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if (contactos.get(position).getFavorito() > 0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    agendaDB.openDatabase();
                    agendaDB.eliminarContacto(contactos.get(position).getID());
                    agendaDB.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return super.getDropDownView(position, convertView, parent);
        }
    }

}
